<?php ?>
<?= $this->Form->create($movie) ?>
<?= $this->Form->control('title');?>
<?= $this->Form->control('director');?>
<div class="input date">
    <label>Date de sortie</label>
    <?= $this->Form->text('releasedate',['label'=>'date de sortie','type'=>'date', 'value'=> (!empty($movie->releasedate))? $movie->releasedate->i18nFormat('yyyy-MM-dd') : '']);?>
</div>
<?= $this->Form->control('duration');?>
<?= $this->Form->control('synopsis');?>
<?= $this->Form->button('Ajouter');?>
<?= $this->Form->end() ?>
