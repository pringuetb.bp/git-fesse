<?php ?>
<h1> page add movie</h1>

<?= $this->Form->create($new) ?>
<?= $this->Form->control('title');?>
<?= $this->Form->control('poster');?>
<?= $this->Form->control('director');?>
<div class="input date">
    <label>Date de sortie</label>
    <?= $this->Form->timestamp('releasedate',['placeholder'=>'jj/mm/date','type'=>'date']);?>
</div>
<?= $this->Form->control('duration');?>
<?= $this->Form->control('synopsis');?>
<?= $this->Form->button('Ajouter');?>
<?= $this->Form->end() ?>
