<?php

namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MoviesTable extends Table{
    public function initialize(array $config)
    {
        $this->addBehavior('timestamp');
    }

    public function validationDefault(Validator $v)
    {
        $v->notEmpty('title')
            ->maxLength('title',300)
            ->allowEmpty('director')
            ->maxLength('director',300)
            ->allowEmpty('poster')
            ->allowEmpty('duration')
            ->allowEmpty('releasedate')
            ->allowEmpty('synopsys');
            return $v;
    }
}
