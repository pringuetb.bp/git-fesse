<?php
namespace App\Controller;
use App\Model\Entity\Movies;
use Cake\Http\Exception\NotFoundException;

class MoviesController extends AppController{

    public function infomovie(){
        $q = $this->Movies->find();
        $this->set(compact('q'));
    }

    public  function viewmovie($id){
        $one = $this->Movies->get($id);
        $this->set('movie', $one);
    }
    public function modifymovie($id){
        //Recuperer les données
        $movie = $this->Movies->get($id);

        if($this->request->is(['post', 'put'])){
            $this->Movies->patchEntity($movie, $this->request->getData());
            if($this->Movies->save($movie)){
                $this->Flash->success('Modification ok');
                return $this->redirect(['action' => 'infomovie', $movie ->id]);
            }
            $this->Flash->error('modif plantée');
        }

        $this->set(compact('movie'));
    }

    public function addmovie(){
        $new = $this->Movies->newEntity();
        if ($this->request->is('post')){
            $new = $this->Movies->patchEntity($new, $this->request->getData());
            if ($this->Movies->save($new)){
                $this->Flash->success('ok');
                return $this->redirect(['action'=>'infomovie']);
            };
            $this->Flash->error('brrrr');
        }
        $this->set(compact('new'));
    }
    public function delete($id){

        if($this->request->is(['post', 'delete'])){
            $movie = $this->Movies->delete->get($id);

            if($this->Movies->delete($movie)){
                $this->Flash->success('adieu');
                return $this->redirect(['action' => 'infomovie']);
            }else{
                $this->Flash->error('sup plantée');
                return $this->redirect(['action' => 'view', $id]);
            }
        }else{
            throw new NotFoundException('Methode interdite');
        }
    }

}
